package com.homework.simpletodo.utils

import android.content.res.Resources
import com.homework.simpletodo.R

class PreferenceKeys(resources: Resources) {
    internal val night_mode_pref_key: String = resources.getString(R.string.night_mode_pref_key)
}
