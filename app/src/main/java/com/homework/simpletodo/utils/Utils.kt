package com.homework.simpletodo.utils

import java.text.SimpleDateFormat
import java.util.*


object Utils {
    fun formatDate(formatString: String, dateToFormat: Date): String {
        val simpleDateFormat = SimpleDateFormat(formatString)
        return simpleDateFormat.format(dateToFormat)
    }
}
