package com.homework.simpletodo.data

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Item(var todo: String = "",
                var hasReminder: Boolean = false,
                var date: Date? = null,
                var color: Int = 1,
                @PrimaryKey var indent: String = "") : RealmObject() {
    companion object {
        val PRIMARRY_KEY: String = "indent"
    }
}