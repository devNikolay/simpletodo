package com.homework.simpletodo.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import com.homework.simpletodo.R
import com.homework.simpletodo.data.Item
import com.homework.simpletodo.service.TodoNotificationService
import io.realm.Realm
import kotlinx.android.synthetic.main.reminder_layout.*
import java.util.*

class ReminderActivity : AppCompatActivity() {
    private var snoozeOptionsArray: Array<String>? = null
    lateinit private var mRealm: Realm
    private var mToDoItems: ArrayList<Item>? = null
    private var mItem: Item? = null
    internal var theme: String? = null

    companion object {
        val EXIT = "exit"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        theme = getSharedPreferences(MainActivity.THEME_PREFERENCES, Context.MODE_PRIVATE).getString(MainActivity.THEME_SAVED, MainActivity.LIGHTTHEME)
        if (theme == MainActivity.LIGHTTHEME) {
            setTheme(R.style.CustomStyle_LightTheme)
        } else {
            setTheme(R.style.CustomStyle_DarkTheme)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.reminder_layout)
        mRealm = Realm.getDefaultInstance()
        mToDoItems = ArrayList(mRealm.where(Item::class.java)?.findAll())

        setSupportActionBar(findViewById(R.id.toolbar) as Toolbar)


        val i = intent
        val id = i.getSerializableExtra(TodoNotificationService.TODOUUID) as UUID
        mItem = mToDoItems!!.firstOrNull { it.indent == id.toString() }

        snoozeOptionsArray = resources.getStringArray(R.array.snooze_options)

        toDoReminderTextViewBody!!.text = mItem!!.todo

        if (theme == MainActivity.LIGHTTHEME) {
            reminderViewSnoozeTextView!!.setTextColor(ContextCompat.getColor(ReminderActivity@ this, R.color.secondary_text))
        } else {
            reminderViewSnoozeTextView!!.setTextColor(Color.WHITE)
            reminderViewSnoozeTextView!!.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_snooze_white_24dp, 0, 0, 0)
        }

        toDoReminderRemoveButton!!.setOnClickListener {
            mToDoItems!!.remove(mItem!!)
            changeOccurred()
            saveData()
            closeApp()
        }


        val adapter = ArrayAdapter(this, R.layout.spinner_text_view, snoozeOptionsArray!!)
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)

        todoReminderSnoozeSpinner!!.adapter = adapter

    }

    private fun closeApp() {
        val i = Intent(this@ReminderActivity, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        //        i.putExtra(EXIT, true);
        val sharedPreferences = getSharedPreferences(MainActivity.SHARED_PREF_DATA_SET_CHANGED, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(EXIT, true)
        editor.apply()
        startActivity(i)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_reminder, menu)
        return true
    }

    private fun changeOccurred() {
        val sharedPreferences = getSharedPreferences(MainActivity.SHARED_PREF_DATA_SET_CHANGED, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(MainActivity.CHANGE_OCCURED, true)
        editor.apply()
    }

    private fun addTimeToDate(mins: Int): Date {
        val date = Date()
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.MINUTE, mins)
        return calendar.time
    }


    private fun valueFromSpinner(): Int {
        when (todoReminderSnoozeSpinner!!.selectedItemPosition) {
            0 -> return 10
            1 -> return 30
            2 -> return 60
            else -> return 0
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.toDoReminderDoneMenuItem -> {
                val date = addTimeToDate(valueFromSpinner())
                mItem!!.date = date
                mItem!!.hasReminder = true
                changeOccurred()
                saveData()
                closeApp()
                //foo
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun saveData() {
        mRealm.copyToRealm(mToDoItems)
        mRealm.commitTransaction()
    }
}
