package com.homework.simpletodo.activities

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.homework.simpletodo.R
import kotlinx.android.synthetic.main.about_layout.*
import kotlinx.android.synthetic.main.activity_settings.*

class AboutActivity : AppCompatActivity() {

    private var appVersion = "1.0"
    internal var theme: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        theme = getSharedPreferences(MainActivity.THEME_PREFERENCES, Context.MODE_PRIVATE).getString(MainActivity.THEME_SAVED, MainActivity.LIGHTTHEME)
        if (theme == MainActivity.DARKTHEME) {
            setTheme(R.style.CustomStyle_DarkTheme)
        } else {
            setTheme(R.style.CustomStyle_LightTheme)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.about_layout)

        val backArrow = ContextCompat.getDrawable(AboutActivity@ this, R.drawable.abc_ic_ab_back_material)
        backArrow?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
        try {
            val info = packageManager.getPackageInfo(packageName, 0)
            appVersion = info.versionName
        } catch (e: Exception) {
            e.printStackTrace()
        }


        aboutVersionTextView.text = resources.getString(R.string.app_version) + appVersion
        aboutContactMe.setOnClickListener {

        }

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(backArrow)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                if (NavUtils.getParentActivityName(this) != null) {
                    NavUtils.navigateUpFromSameTask(this)
                }
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}