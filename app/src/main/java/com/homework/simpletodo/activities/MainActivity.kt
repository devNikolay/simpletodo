package com.homework.simpletodo.activities

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.text.format.DateFormat
import android.view.*
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.homework.simpletodo.R
import com.homework.simpletodo.data.Item
import com.homework.simpletodo.recycle.CustomRecyclerScrollViewListener
import com.homework.simpletodo.recycle.ItemTouchHelperClass
import com.homework.simpletodo.service.TodoNotificationService
import com.homework.simpletodo.utils.Utils.formatDate
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_settings.*
import java.util.*

class MainActivity : AppCompatActivity() {
    private var mToDoItemsArrayList: ArrayList<Item>? = null
    private var adapter: BasicListAdapter? = null
    private var mJustDeletedToDoItem: Item? = null
    private var mIndexOfDeletedToDoItem: Int = 0
    var itemTouchHelper: ItemTouchHelper? = null
    private var customRecyclerScrollViewListener: CustomRecyclerScrollViewListener? = null
    private var mTheme = -1
    private var theme = "name_of_the_theme"
    private var mItemDeleted: Boolean = true
    lateinit private var mRealm: Realm

    companion object {
        val TODOITEM = "simpletodo.MainActivity"
        private val REQUEST_ID_TODO_ITEM = 100
        val DATE_TIME_FORMAT_12_HOUR = "MMM d, yyyy  h:mm a"
        val DATE_TIME_FORMAT_24_HOUR = "MMM d, yyyy  k:mm"
        val SHARED_PREF_DATA_SET_CHANGED = "simpletodo.datasetchanged"
        val CHANGE_OCCURED = "simpletodo.changeoccured"
        val THEME_PREFERENCES = "simpletodo.themepref"
        val RECREATE_ACTIVITY = "simpletodo.recreateactivity"
        val THEME_SAVED = "simpletodo.savedtheme"
        val DARKTHEME = "simpletodo.darktheme"
        val LIGHTTHEME = "simpletodo.lighttheme"
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences = getSharedPreferences(SHARED_PREF_DATA_SET_CHANGED, Context.MODE_PRIVATE)
        if (sharedPreferences.getBoolean(ReminderActivity.EXIT, false)) {
            val editor = sharedPreferences.edit()
            editor.putBoolean(ReminderActivity.EXIT, false)
            editor.apply()
            finish()
        }
        /*
        We need to do this, as this activity's onCreate won't be called when coming back from SettingsActivity,
        thus our changes to dark/light mode won't take place, as the setContentView() is not called again.
        So, inside our SettingsFragment, whenever the checkbox's value is changed, in our shared preferences,
        we mark our recreate_activity key as true.

        Note: the recreate_key's value is changed to false before calling recreate(), or we would have ended up in an infinite loop,
        as onResume() will be called on recreation, which will again call recreate() and so on....
        and get an ANR

         */
        if (getSharedPreferences(THEME_PREFERENCES, Context.MODE_PRIVATE).getBoolean(RECREATE_ACTIVITY, false)) {
            val editor = getSharedPreferences(THEME_PREFERENCES, Context.MODE_PRIVATE).edit()
            editor.putBoolean(RECREATE_ACTIVITY, false)
            editor.apply()
            recreate()
        }
    }

    private fun setAlarms() {
        if (mToDoItemsArrayList != null) {
            for (item in mToDoItemsArrayList!!) {
                if (item.hasReminder && item.date != null) {
                    if (item.date!!.before(Date())) {
                        item.date = null
                        continue
                    }
                    val i = Intent(this, TodoNotificationService::class.java)
                    i.putExtra(TodoNotificationService.TODOUUID, item.indent)
                    i.putExtra(TodoNotificationService.TODOTEXT, item.todo)
                    createAlarm(i, item.indent.hashCode(), item.date!!.time)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        mRealm = Realm.getDefaultInstance()
        //We recover the theme we've set and setTheme accordingly
        theme = getSharedPreferences(THEME_PREFERENCES, Context.MODE_PRIVATE).getString(THEME_SAVED, LIGHTTHEME)

        if (theme == LIGHTTHEME) {
            mTheme = R.style.CustomStyle_LightTheme
        } else {
            mTheme = R.style.CustomStyle_DarkTheme
        }
        this.setTheme(mTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val sharedPreferences = getSharedPreferences(SHARED_PREF_DATA_SET_CHANGED, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(CHANGE_OCCURED, false)
        editor.apply()

        mToDoItemsArrayList = loadData()
        adapter = BasicListAdapter(mToDoItemsArrayList!!)
        setAlarms()

        setSupportActionBar(toolbar)

        addToDoItemFAB.setOnClickListener {
            val newTodo = Intent(this@MainActivity, AddToDoActivity::class.java)
            val color = ColorGenerator.MATERIAL.randomColor
            mRealm.executeTransaction {
                val item = mRealm.createObject(Item::class.java, UUID.randomUUID().toString())
                item.color = color
                newTodo.putExtra(TODOITEM, item.indent)
            }
            startActivityForResult(newTodo, REQUEST_ID_TODO_ITEM)
        }

        if (theme == LIGHTTHEME) {
            toDoRecyclerView!!.setBackgroundColor(ContextCompat.getColor(MainActivity@ this, R.color.primary_lightest))
        }
        toDoRecyclerView!!.setEmptyView(findViewById(R.id.toDoEmptyView))
        toDoRecyclerView!!.setHasFixedSize(true)
        toDoRecyclerView!!.itemAnimator = DefaultItemAnimator()
        toDoRecyclerView!!.layoutManager = LinearLayoutManager(this)

        customRecyclerScrollViewListener = object : CustomRecyclerScrollViewListener() {
            override fun show() {
                addToDoItemFAB!!.animate().translationY(0f).setInterpolator(DecelerateInterpolator(2f)).start()
            }

            override fun hide() {
                val lp = addToDoItemFAB!!.layoutParams as CoordinatorLayout.LayoutParams
                val fabMargin = lp.bottomMargin
                addToDoItemFAB!!.animate().translationY((addToDoItemFAB!!.height + fabMargin).toFloat()).setInterpolator(AccelerateInterpolator(2.0f)).start()
            }
        }
        toDoRecyclerView!!.addOnScrollListener(customRecyclerScrollViewListener)


        val callback = ItemTouchHelperClass(adapter!!)
        itemTouchHelper = ItemTouchHelper(callback)
        itemTouchHelper!!.attachToRecyclerView(toDoRecyclerView)

        toDoRecyclerView!!.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.aboutMeMenuItem -> {
                val i = Intent(this, AboutActivity::class.java)
                startActivity(i)
                return true
            }
            R.id.preferences -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (resultCode != Activity.RESULT_CANCELED && requestCode == REQUEST_ID_TODO_ITEM) {
            val item = mRealm.where(Item::class.java).equalTo(Item.PRIMARRY_KEY, data.getStringExtra(TODOITEM)).findFirst()
            if (item.todo.isEmpty()) {
                return
            }
            var existed = false

            if (item.hasReminder && item.date != null) {
                val i = Intent(this, TodoNotificationService::class.java)
                i.putExtra(TodoNotificationService.TODOTEXT, item.todo)
                i.putExtra(TodoNotificationService.TODOUUID, item.indent)
                createAlarm(i, item.indent.hashCode(), item.date!!.time)
            }

            for (i in mToDoItemsArrayList!!.indices) {
                if (item.indent == mToDoItemsArrayList!![i].indent) {
                    mToDoItemsArrayList!![i] = item
                    existed = true
                    adapter!!.notifyItemChanged(i)
                    break
                }
            }
            if (!existed) {
                addToDataStore(item)
            }
        }
    }

    private val alarmManager: AlarmManager
        get() = getSystemService(Context.ALARM_SERVICE) as AlarmManager

    private fun doesPendingIntentExist(i: Intent, requestCode: Int): Boolean {
        val pi = PendingIntent.getService(this, requestCode, i, PendingIntent.FLAG_NO_CREATE)
        return pi != null
    }

    private fun createAlarm(i: Intent, requestCode: Int, timeInMillis: Long) {
        val am = alarmManager
        val pi = PendingIntent.getService(this, requestCode, i, PendingIntent.FLAG_UPDATE_CURRENT)
        am.set(AlarmManager.RTC_WAKEUP, timeInMillis, pi)
    }

    private fun deleteAlarm(i: Intent, requestCode: Int) {
        if (doesPendingIntentExist(i, requestCode)) {
            val pi = PendingIntent.getService(this, requestCode, i, PendingIntent.FLAG_NO_CREATE)
            pi.cancel()
            alarmManager.cancel(pi)
        }
    }

    private fun addToDataStore(item: Item) {
        mToDoItemsArrayList!!.add(item)
        adapter!!.notifyItemInserted(mToDoItemsArrayList!!.size - 1)

    }

    inner class BasicListAdapter internal constructor(private val items: ArrayList<Item>) : RecyclerView.Adapter<BasicListAdapter.ViewHolder>(), ItemTouchHelperClass.ItemTouchHelperAdapter {

        override fun onItemMoved(fromPosition: Int, toPosition: Int) {
            if (fromPosition < toPosition) {
                for (i in fromPosition..toPosition - 1) {
                    Collections.swap(items, i, i + 1)
                }
            } else {
                for (i in fromPosition downTo toPosition + 1) {
                    Collections.swap(items, i, i - 1)
                }
            }
            notifyItemMoved(fromPosition, toPosition)
        }

        override fun onItemRemoved(position: Int) {
            mJustDeletedToDoItem = items.removeAt(position)
            mIndexOfDeletedToDoItem = position
            val i = Intent(this@MainActivity, TodoNotificationService::class.java)
            deleteAlarm(i, mJustDeletedToDoItem!!.indent.hashCode())
            notifyItemRemoved(position)
            createUndoSnackBar()
            deleteItemFromRealm()
        }

        private fun createUndoSnackBar() {
            mItemDeleted = true
            Snackbar.make(myCoordinatorLayout!!, getText(R.string.deleted_todo), Snackbar.LENGTH_LONG).setAction(getText(R.string.undo_deleting)) {
                mItemDeleted = false
                items.add(mIndexOfDeletedToDoItem, mJustDeletedToDoItem!!)
                if (mJustDeletedToDoItem!!.date != null && mJustDeletedToDoItem!!.hasReminder) {
                    val i = Intent(this@MainActivity, TodoNotificationService::class.java)
                    i.putExtra(TodoNotificationService.TODOTEXT, mJustDeletedToDoItem!!.todo)
                    i.putExtra(TodoNotificationService.TODOUUID, mJustDeletedToDoItem!!.indent)
                    createAlarm(i, mJustDeletedToDoItem!!.indent.hashCode(), mJustDeletedToDoItem!!.date!!.time)
                }
                notifyItemInserted(mIndexOfDeletedToDoItem)
            }.show()
        }

        private fun deleteItemFromRealm() {
            val handler = Handler()
            handler.postDelayed({
                if (mItemDeleted) {
                    mRealm.executeTransaction {
                        val result: Item = mRealm.where(Item::class.java).equalTo(Item.PRIMARRY_KEY, mJustDeletedToDoItem!!.indent).findFirst()
                        result.deleteFromRealm()
                    }
                }
            }, 4000)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.list_circle_try, parent, false)
            return ViewHolder(v)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = items[position]
            val sharedPreferences = getSharedPreferences(THEME_PREFERENCES, Context.MODE_PRIVATE)
            //Background color for each to-do item. Necessary for night/day mode
            val bgColor: Int
            //color of title text in our to-do item. White for night mode, dark gray for day mode
            val todoTextColor: Int
            if (sharedPreferences.getString(THEME_SAVED, LIGHTTHEME) == LIGHTTHEME) {
                bgColor = Color.WHITE
                todoTextColor = ContextCompat.getColor(this@MainActivity, R.color.secondary_text)
            } else {
                bgColor = Color.DKGRAY
                todoTextColor = Color.WHITE
            }
            holder.linearLayout.setBackgroundColor(bgColor)

            if (item.hasReminder && item.date != null) {
                holder.mToDoTextView.maxLines = 1
                holder.mTimeTextView.visibility = View.VISIBLE
            } else {
                holder.mTimeTextView.visibility = View.GONE
                holder.mToDoTextView.maxLines = 2
            }
            holder.mToDoTextView.text = item.todo
            holder.mToDoTextView.setTextColor(todoTextColor)
            val myDrawable = TextDrawable.builder()
                    .beginConfig()
                    .textColor(Color.WHITE)
                    .useFont(Typeface.DEFAULT)
                    .toUpperCase().endConfig()
                    .buildRound(item.todo.substring(0, 1), item.color)

            holder.mColorImageView.setImageDrawable(myDrawable)
            val timeToShow: String
            if (DateFormat.is24HourFormat(this@MainActivity)) {
                timeToShow = formatDate(DATE_TIME_FORMAT_24_HOUR, item.date!!)
            } else {
                timeToShow = formatDate(DATE_TIME_FORMAT_12_HOUR, item.date!!)
            }
            holder.mTimeTextView.text = timeToShow
        }

        override fun getItemCount(): Int {
            return items.size
        }

        inner class ViewHolder
        (mView: View) : RecyclerView.ViewHolder(mView) {
            internal var linearLayout: LinearLayout
            internal var mToDoTextView: TextView
            internal var mColorImageView: ImageView
            internal var mTimeTextView: TextView

            init {
                mView.setOnClickListener {
                    val item = items[this@ViewHolder.adapterPosition]
                    val i = Intent(this@MainActivity, AddToDoActivity::class.java)
                    i.putExtra(TODOITEM, item.indent)
                    startActivityForResult(i, REQUEST_ID_TODO_ITEM)
                }
                mToDoTextView = mView.findViewById(R.id.toDoListItemTextview) as TextView
                mTimeTextView = mView.findViewById(R.id.todoListItemTimeTextView) as TextView
                mColorImageView = mView.findViewById(R.id.toDoListItemColorImageView) as ImageView
                linearLayout = mView.findViewById(R.id.listItemLinearLayout) as LinearLayout
            }
        }
    }

    private fun loadData(): ArrayList<Item>? {
        return ArrayList(mRealm.where(Item::class.java).findAll())
    }

    override fun onDestroy() {
        super.onDestroy()
        mRealm.close()
        toDoRecyclerView!!.removeOnScrollListener(customRecyclerScrollViewListener)
    }
}


