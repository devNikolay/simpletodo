package com.homework.simpletodo.activities

import android.animation.Animator
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateFormat
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.homework.simpletodo.R
import com.homework.simpletodo.data.Item
import com.homework.simpletodo.utils.Utils.formatDate
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.activity_todo_test.*
import java.util.*

class AddToDoActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private var mUserToDoItem: Item? = null
    private var mUserEnteredText: String? = null
    private var mUserHasReminder: Boolean = false
    private var mUserReminderDate: Date? = null
    private var mUserColor: Int = 0
    private var theme: String? = null
    lateinit private var mRealm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        mRealm = Realm.getDefaultInstance()
        theme = getSharedPreferences(MainActivity.THEME_PREFERENCES, Context.MODE_PRIVATE).getString(MainActivity.THEME_SAVED, MainActivity.LIGHTTHEME)
        if (theme == MainActivity.LIGHTTHEME) {
            setTheme(R.style.CustomStyle_LightTheme)
        } else {
            setTheme(R.style.CustomStyle_DarkTheme)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todo_test)

        //Show an X in place of <-
//        val cross = resources.getDrawable(R.drawable.ic_clear_white_24dp)
        val cross = ContextCompat.getDrawable(AddToDoActivity@ this, R.drawable.ic_clear_white_24dp)
        cross?.setColorFilter(ContextCompat.getColor(AddToDoActivity@ this, R.color.icons), PorterDuff.Mode.SRC_ATOP)

        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            setupActionBar(cross)
        }


        mUserToDoItem = mRealm.where(Item::class.java).equalTo(Item.PRIMARRY_KEY, intent.getStringExtra(MainActivity.TODOITEM)).findFirst()

        mUserEnteredText = mUserToDoItem!!.todo
        mUserHasReminder = mUserToDoItem!!.hasReminder
        mUserReminderDate = mUserToDoItem!!.date
        mUserColor = mUserToDoItem!!.color


        if (theme == MainActivity.DARKTHEME) {
            userToDoReminderIconImageButton.setImageDrawable(ContextCompat.getDrawable(AddToDoActivity@ this, R.drawable.ic_alarm_add_white_24dp))
            userToDoRemindMeTextView.setTextColor(Color.WHITE)
        }
        todoReminderAndDateContainerLayout.setOnClickListener { hideKeyboard(userToDoEditText!!) }

        if (mUserHasReminder && mUserReminderDate != null) {
            setReminderTextView()
            setEnterDateLayoutVisibleWithAnimations(true)
        }
        if (mUserReminderDate == null) {
            toDoHasDateSwitchCompat!!.isChecked = false
            newToDoDateTimeReminderTextView!!.visibility = View.INVISIBLE
        }
        userToDoEditText!!.requestFocus()
        userToDoEditText!!.setText(mUserEnteredText)
        val imm = this.getSystemService(android.content.Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
        userToDoEditText!!.setSelection(userToDoEditText!!.length())


        userToDoEditText!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                mUserEnteredText = s.toString()

            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        setEnterDateLayoutVisible(toDoHasDateSwitchCompat!!.isChecked)

        toDoHasDateSwitchCompat!!.isChecked = mUserHasReminder && mUserReminderDate != null
        toDoHasDateSwitchCompat!!.setOnCheckedChangeListener { _, isChecked ->
            if (!isChecked) {
                mUserReminderDate = null
            }
            mUserHasReminder = isChecked
            setDateAndTimeEditText()
            setEnterDateLayoutVisibleWithAnimations(isChecked)
            hideKeyboard(userToDoEditText!!)
        }


        makeToDoFloatingActionButton!!.setOnClickListener {
            if (mUserReminderDate != null && mUserReminderDate!!.before(Date())) {
                makeResult(Activity.RESULT_CANCELED)
            } else {
                makeResult(Activity.RESULT_OK)
            }
            hideKeyboard(userToDoEditText!!)
            finish()
        }


        newTodoDateEditText!!.setOnClickListener {
            val date: Date
            hideKeyboard(userToDoEditText!!)
            if (mUserToDoItem!!.date != null) {
                date = mUserReminderDate!!
            } else {
                date = Date()
            }
            val calendar = Calendar.getInstance()
            calendar.time = date
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog.newInstance(this, year, month, day)
            if (theme == MainActivity.DARKTHEME) {
                datePickerDialog.isThemeDark = true
            }
            datePickerDialog.show(fragmentManager, "DateFragment")
        }


        newTodoTimeEditText!!.setOnClickListener {
            val date: Date
            hideKeyboard(userToDoEditText!!)
            if (mUserToDoItem!!.date != null) {
                date = mUserReminderDate!!
            } else {
                date = Date()
            }
            val calendar = Calendar.getInstance()
            calendar.time = date
            val hour = calendar.get(Calendar.HOUR_OF_DAY)
            val minute = calendar.get(Calendar.MINUTE)

            val timePickerDialog = TimePickerDialog.newInstance(this, hour, minute, DateFormat.is24HourFormat(this))
            if (theme == MainActivity.DARKTHEME) {
                timePickerDialog.isThemeDark = true
            }
            timePickerDialog.show(fragmentManager, "TimeFragment")
        }

        setDateAndTimeEditText()
    }

    private fun setupActionBar(cross: Drawable?) {
        supportActionBar!!.elevation = 0f
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(cross)
    }

    private fun setDateAndTimeEditText() {

        if (mUserToDoItem!!.hasReminder && mUserReminderDate != null) {
            val userDate = formatDate("d MMM, yyyy", mUserReminderDate!!)
            val formatToUse: String
            if (DateFormat.is24HourFormat(this)) {
                formatToUse = "k:mm"
            } else {
                formatToUse = "h:mm a"

            }
            val userTime = formatDate(formatToUse, mUserReminderDate!!)
            newTodoTimeEditText!!.setText(userTime)
            newTodoDateEditText!!.setText(userDate)

        } else {
            newTodoDateEditText!!.setText(getString(R.string.date_reminder_default))
            val time24 = DateFormat.is24HourFormat(this)
            val cal = Calendar.getInstance()
            if (time24) {
                cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 1)
            } else {
                cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) + 1)
            }
            cal.set(Calendar.MINUTE, 0)
            mUserReminderDate = cal.time
            val timeString: String
            if (time24) {
                timeString = formatDate("k:mm", mUserReminderDate!!)
            } else {
                timeString = formatDate("h:mm a", mUserReminderDate!!)
            }
            newTodoTimeEditText!!.setText(timeString)
        }
    }

    fun hideKeyboard(et: EditText) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(et.windowToken, 0)
    }


    fun setDate(year: Int, month: Int, day: Int) {
        val calendar = Calendar.getInstance()
        val hour: Int
        val minute: Int

        val reminderCalendar = Calendar.getInstance()
        reminderCalendar.set(year, month, day)

        if (reminderCalendar.before(calendar)) {
            return
        }

        if (mUserReminderDate != null) {
            calendar.time = mUserReminderDate
        }

        if (DateFormat.is24HourFormat(this)) {
            hour = calendar.get(Calendar.HOUR_OF_DAY)
        } else {

            hour = calendar.get(Calendar.HOUR)
        }
        minute = calendar.get(Calendar.MINUTE)

        calendar.set(year, month, day, hour, minute)
        mUserReminderDate = calendar.time
        setReminderTextView()
        //        setDateAndTimeEditText();
        setDateEditText()
    }

    fun setTime(hour: Int, minute: Int) {
        val calendar = Calendar.getInstance()
        if (mUserReminderDate != null) {
            calendar.time = mUserReminderDate
        }

        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        calendar.set(year, month, day, hour, minute, 0)
        mUserReminderDate = calendar.time

        setReminderTextView()
        //        setDateAndTimeEditText();
        setTimeEditText()
    }

    fun setDateEditText() {
        val dateFormat = "d MMM, yyyy"
        newTodoDateEditText!!.setText(formatDate(dateFormat, mUserReminderDate!!))
    }

    fun setTimeEditText() {
        val dateFormat: String
        if (DateFormat.is24HourFormat(this)) {
            dateFormat = "k:mm"
        } else {
            dateFormat = "h:mm a"

        }
        newTodoTimeEditText!!.setText(formatDate(dateFormat, mUserReminderDate!!))
    }

    fun setReminderTextView() {
        if (mUserReminderDate != null) {
            newToDoDateTimeReminderTextView!!.visibility = View.VISIBLE
            val dateToFormat = mUserReminderDate!!
            if (dateToFormat.before(Date())) {
                newToDoDateTimeReminderTextView!!.text = getString(R.string.date_error_check_again)
                newToDoDateTimeReminderTextView!!.setTextColor(Color.RED)
                return
            }
            val dateString = formatDate("d MMM, yyyy", dateToFormat)
            val timeString: String
            var amPmString = ""

            if (DateFormat.is24HourFormat(this)) {
                timeString = formatDate("k:mm", dateToFormat)
            } else {
                timeString = formatDate("h:mm", dateToFormat)
                amPmString = formatDate("a", dateToFormat)
            }
            val finalString = String.format(resources.getString(R.string.remind_date_and_time), dateString, timeString, amPmString)
            newToDoDateTimeReminderTextView!!.setTextColor(ContextCompat.getColor(AddToDoActivity@ this, R.color.secondary_text))
            newToDoDateTimeReminderTextView!!.text = finalString
        } else {
            newToDoDateTimeReminderTextView!!.visibility = View.INVISIBLE

        }
    }

    fun makeResult(result: Int) {
        val i = Intent()
        mRealm.executeTransaction {
            if (mUserEnteredText!!.isNotEmpty()) {
                val capitalizedString = Character.toUpperCase(mUserEnteredText!![0]) + mUserEnteredText!!.substring(1)
                mUserToDoItem!!.todo = capitalizedString
            } else {
                mUserToDoItem!!.todo = mUserEnteredText as String
            }
            if (mUserReminderDate != null) {
                val calendar = Calendar.getInstance()
                calendar.time = mUserReminderDate
                calendar.set(Calendar.SECOND, 0)
                mUserReminderDate = calendar.time
            }
            mUserToDoItem!!.hasReminder = mUserHasReminder
            mUserToDoItem!!.date = mUserReminderDate
            mUserToDoItem!!.color = mUserColor
        }
        i.putExtra(MainActivity.TODOITEM, mUserToDoItem!!.indent)
        setResult(result, i)
    }

    override fun onBackPressed() {
        if (mUserReminderDate!!.before(Date())) {
            mUserToDoItem!!.date = null
        }
        makeResult(Activity.RESULT_OK)
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                if (NavUtils.getParentActivityName(this) != null) {
                    makeResult(Activity.RESULT_CANCELED)
                    NavUtils.navigateUpFromSameTask(this)
                }
                hideKeyboard(userToDoEditText!!)
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onTimeSet(radialPickerLayout: RadialPickerLayout, hour: Int, minute: Int) {
        setTime(hour, minute)
    }

    override fun onDateSet(datePickerDialog: DatePickerDialog, year: Int, month: Int, day: Int) {
        setDate(year, month, day)
    }

    fun setEnterDateLayoutVisible(checked: Boolean) {
        if (checked) {
            toDoEnterDateLinearLayout!!.visibility = View.VISIBLE
        } else {
            toDoEnterDateLinearLayout!!.visibility = View.INVISIBLE
        }
    }

    fun setEnterDateLayoutVisibleWithAnimations(checked: Boolean) {
        if (checked) {
            setReminderTextView()
            toDoEnterDateLinearLayout!!.animate().alpha(1.0f).setDuration(500).setListener(
                    object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {
                            toDoEnterDateLinearLayout!!.visibility = View.VISIBLE
                        }

                        override fun onAnimationEnd(animation: Animator) {
                        }

                        override fun onAnimationCancel(animation: Animator) {
                        }

                        override fun onAnimationRepeat(animation: Animator) {
                        }
                    })
        } else {
            toDoEnterDateLinearLayout!!.animate().alpha(0.0f).setDuration(500).setListener(
                    object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {

                        }

                        override fun onAnimationEnd(animation: Animator) {
                            toDoEnterDateLinearLayout!!.visibility = View.INVISIBLE
                        }

                        override fun onAnimationCancel(animation: Animator) {

                        }

                        override fun onAnimationRepeat(animation: Animator) {

                        }
                    })
        }

    }
}

