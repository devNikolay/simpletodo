package com.homework.simpletodo.service

import android.app.IntentService
import android.content.Context
import android.content.Intent
import com.homework.simpletodo.activities.MainActivity
import com.homework.simpletodo.data.Item
import io.realm.Realm
import java.util.*

class DeleteNotificationService : IntentService("DeleteNotificationService") {

    private var mToDoItems: ArrayList<Item>? = null
    private var mItem: Item? = null
    lateinit private var mRealm: Realm

    override fun onHandleIntent(intent: Intent) {
        mRealm = Realm.getDefaultInstance()
        val todoID = intent.getSerializableExtra(TodoNotificationService.TODOUUID) as UUID

        mToDoItems = loadData()
        if (mToDoItems != null) {
            for (item in mToDoItems!!) {
                if (item.indent == todoID.toString()) {
                    mItem = item
                    break
                }
            }

            if (mItem != null) {
                mToDoItems!!.remove(mItem!!)
                mRealm.executeTransactionAsync {
                    val result: Item = mRealm.where(Item::class.java).equalTo(Item.PRIMARRY_KEY, mItem!!.indent).findFirst()
                    result.deleteFromRealm()
                }
                dataChanged()
            }
        }
    }

    private fun dataChanged() {
        val sharedPreferences = getSharedPreferences(MainActivity.SHARED_PREF_DATA_SET_CHANGED, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(MainActivity.CHANGE_OCCURED, true)
        editor.apply()
    }

    override fun onDestroy() {
        super.onDestroy()
        mRealm.close()
    }

    private fun loadData(): ArrayList<Item>? {
        return ArrayList(mRealm.where(Item::class.java).findAll())
    }
}
