package com.homework.simpletodo

import android.app.Application
import io.realm.Realm

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // Initialize Realm
        Realm.init(this)
    }
}